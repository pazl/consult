package com.prophet.consult;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Autho changqi.wu
 * @Date 路在脚下，使劲踩！
 */
@SpringBootApplication
public class ConsultServer {

    public static void main(String[] args) {
        SpringApplication.run(ConsultServer.class,args);
    }

}
